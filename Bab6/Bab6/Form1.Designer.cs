﻿namespace Bab6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btnload = new System.Windows.Forms.Button();
            this.btngrayscale = new System.Windows.Forms.Button();
            this.btnhisogram = new System.Windows.Forms.Button();
            this.btncdf = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnload
            // 
            this.btnload.Location = new System.Drawing.Point(12, 12);
            this.btnload.Name = "btnload";
            this.btnload.Size = new System.Drawing.Size(95, 36);
            this.btnload.TabIndex = 0;
            this.btnload.Text = "LOAD";
            this.btnload.UseVisualStyleBackColor = true;
            this.btnload.Click += new System.EventHandler(this.btnload_Click);
            // 
            // btngrayscale
            // 
            this.btngrayscale.Location = new System.Drawing.Point(123, 12);
            this.btngrayscale.Name = "btngrayscale";
            this.btngrayscale.Size = new System.Drawing.Size(96, 36);
            this.btngrayscale.TabIndex = 1;
            this.btngrayscale.Text = "GrayScale";
            this.btngrayscale.UseVisualStyleBackColor = true;
            this.btngrayscale.Click += new System.EventHandler(this.btngrayscale_Click);
            // 
            // btnhisogram
            // 
            this.btnhisogram.Location = new System.Drawing.Point(235, 12);
            this.btnhisogram.Name = "btnhisogram";
            this.btnhisogram.Size = new System.Drawing.Size(91, 36);
            this.btnhisogram.TabIndex = 2;
            this.btnhisogram.Text = "Histogram";
            this.btnhisogram.UseVisualStyleBackColor = true;
            this.btnhisogram.Click += new System.EventHandler(this.btnhisogram_Click);
            // 
            // btncdf
            // 
            this.btncdf.Location = new System.Drawing.Point(345, 12);
            this.btncdf.Name = "btncdf";
            this.btncdf.Size = new System.Drawing.Size(89, 36);
            this.btncdf.TabIndex = 3;
            this.btncdf.Text = "CDF";
            this.btncdf.UseVisualStyleBackColor = true;
            this.btncdf.Click += new System.EventHandler(this.btncdf_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 71);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(314, 326);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(363, 71);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(462, 326);
            this.chart1.TabIndex = 5;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 412);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btncdf);
            this.Controls.Add(this.btnhisogram);
            this.Controls.Add(this.btngrayscale);
            this.Controls.Add(this.btnload);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnload;
        private System.Windows.Forms.Button btngrayscale;
        private System.Windows.Forms.Button btnhisogram;
        private System.Windows.Forms.Button btncdf;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

